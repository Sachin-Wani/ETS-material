1996��1�� ��������

1. It's such a nice day. Why don't we have lunch outside?
OK. Bur let's find someplace that's not too noisy.
What does the man mean?

2. Look at the time. I'm going to miss my bus.
Don't worry. I'll drive you to the stop. And if the us has already left I can get you to your apartment.
What does the woman mean?

3. Can you recommend a hotel in New York?
Well, I can think of several. What's your budget like?
What does the man imply?

4. Any messages for me?
Someone did call. But there was so much static I couldn't make out what he was saying.
  What does the woman mean?

5. Didn't George give a great speech?
Are you serious?
What can be inferred about the woman?

6. Can I open a checking account here?
I'm sorry. You'll have to step over to the manager's desk.
What does the woman imply?

7. Now that I've finished my exams. I'm going to relax and go to a movie tonight.
Lucky you! I've still got two finals to take.
What can be inferred about the woman?

8. It's very nice of you to give me your tickets for the play.
Please. Don't mention it. I'm going to be out of town this weekend anyway.
What can be inferred about the man?

9. I almost forgot. I still owe you ten dollars from the other night. 
Do you have change for a twenty?
Oh--. Not at the moment. Let's just straighten it out some other time.
What does the woman mean?

10. Hey! What happened to all that food we bought? The refrigerator is empty.
My little brother with a big appetite was here. He really combed these out.
What does the man mean?

11. I'm taking Prof. Bam's course next semester. Anything I need to know about it?
   If I were you. I'll take careful notes. Her exams are based on her lectures. 
   What advice does the man give the woman?

12. I'm glad you finally decided to go skiing with us next week.
I still have to get my supervisor to agree to it.
What does the woman imply?

13. Did you see the college newspaper? They did a story on our voter registration campaign.
I did. Maybe it'll spark some interest on campus. Without more volunteers we'll never meet our goal.
   What does the man mean?

14. Have you net the new sales manager yet?
We've been introduced about three times, he seems a little forgetful.
What can be inferred about the sales manager?

15. Excuse me, but do you happen to have some change for the paring meter?
No. But if you go into the restaurant you'll probably be able to change a dollar bill.
What does the woman want to do?

16. I am going to tell that neighbor of mine to turn down that music once and for all.
I see why you are angry. But I've always found that the polite route is the most effective.
What does the man mean?

17. I love your new sofa. But why don't you put it over there under the window?
Oh, but the plants are doing well on the table there.
What does the man imply?

18. At the rate of its being used, the copier is not going to make it through the rest of the year.
The year? It's supposed to be good for five.
What does the woman say about the copier?

19. I've been invited to a dinner party at Janet's. Do you think should bring something?
You could pick up a cake. Chocolate is her favorite.
What does the woman suggest the man do?

20. I'm sorry I missed your soccer game. But I had the flu.
Don't worry about it. We couldn't have played worse.
What does the woman mean?

21. Do you feel like watching the evening news?
You ought to stay away from me. I have a bad could and wouldn't want you to catch it.
What does the woman mean?.

22. Hew! It's a real scorcher today.
And the forecasters are saying there is no end in sight.
What does the man imply?

23. Uh-uh. Look I'm going to be a little late for class. I hope Prof. Clark does start on time today.
Are you kidding? You can set your watch by the start of his class.
What can be inferred about Prof. Clark?

24. Are you keeping count on the news from home since you've been here?
I've been getting weekly updates.
What does the man mean?

25. Well, what did you think of the theater director?
You mean Emily Thompson? She was away attending a conference.
What does the man mean?

26. That movie was awful. And yet it got such great reviews.
It was hardly worth the price of admission.
What does the man mean?

27. I'm just looking for bike that will me get to the library and back.
With the roll of the way they are you'll need a sturdy one. 
What does the woman imply?

28. I'll take this suit. It fits me really well. And while I amend it I like the shirt and the tie too.
We have some nice socks that match.
What does the woman mean?

29. Sorry I didn't see you on the news. 
Yeah. And it's not likely they are going to rebroadcast it anytime soon.
What does the man imply?

30. The science exhibit is opening today. You are coming with me this afternoon, aren't you?
I have too much work to do for tomorrow.
What does the man mean?

PART B
31-34  Conversation between a student and guidance counselor
*Hello, John. You must be pleased. After all how many students are lucky enough to have been accepted at their first and their second choices?
*Not many I know. But I'm not sure yet which one to choose.
*Well, you seem to have doubts about the state university. But its biology department has a fine reputation. What more could a biology major want?
*Yeah. And they also have internships for seniors. But a friend told me that for the first two years some lectures have a hundred and fifty students. You probably wouldn't get to know any of your teachers.
*Well, you might actually. Because those classes also have small discussion sections. Twice a week, and have no more than twenty students.
*I know. But I've heard that they are usually taught by graduate students. At White Stone College all classes are taught by professors.
*What about Sating? Do you prefer a small town like White Stone? Or a bigger place like the state capital?
*That doesn't matter to me. What I do care about is getting individual attention from the faculty than making friends.
*Look, I've known you for four years now and you seen to be a pretty outgoing person. I don't think you have any trouble making friends at the state university. It sounds to me that you are learning toward White Stone though?
*I am. The only problem is that the White Stone's tuition is really high and I'm not sure I can afford it.
*You could still apply for a student loan or sign up for a work-study program.
*Yeah. I think I'll look into that.

31. Why did the man go to see the woman?
32. What concern does the man have about the state university?
33. What does the man consider to be an advantage of the White Stone College?
34. What is the man likely to do in the near future?

35-39  Conversation between two students.
*Hi, Claire. How does it feel to be back on campus?
*Keach, hi. Well, to tell you the truth, I have mixed feelings. 
*Oh, why?
*I have this great summer job that I really hated to leave. I worked at the wild life research center in Maryland.
*That makes sense for a genetic major. What did you do? Clean the cages?
*This is a wild life center, not a zoo. This place breeds endangered species and tries to prepare them for life in the wild.
*You mean the endangered species like the tiger and the panda?
*Well, endangered species, yes. But not tigers or pandas. I was working with whooping cranes and sandtail cranes. It was really neat. I taught the baby crane how to eat and drink, and I help the vet to give medical check-ups.
*I can see why it was hard to leave that job. But how did you teach a bird how to eat and drink?
*We covered ourselves up with clothes and used puppets made out of stuffed cranes to show the baby chicks what to do. Then the chicks copied what the puppets did.
*Clothes? Puppets? Sounds like fun.
*It was. The clothes and puppets are the key tool of this. We all covered ourselves up, the scientists, the vets, the genial stock, everybody. You see, baby cranes will become attached to their care-takers.
*So if the care-taker is a person, the crane will stay in places where people are.
*Yeah. And their chances for survival aren't very good. But by covering ourselves and by using birds puppets the chicks are more likely to seek out other birds rather than people. And their transition to the wild has a better chance of being successful.
*A chance of being successful? Hasn't this been done before?
*It's been done with sandtail cranes and everyone is optimistic about its work with whooping cranes too.
*Yeah. It's exciting, isn't that?

35. Why does the woman say she has a mixed feelings?
36. What was the woman's job?
37. What is the goal of the wild life center?
38. Why does the man mention the tigers and pandas?
39. Why do the staff members cover themselves with clothes as they work?

PART C  TALKS
40 to 42 A lecture in a architectural design course.
In 1871 the first passenger elevators were used in office buildings and allowed architects to build higher than people could comfortably walk. Another innovation was in building technique. In 1885 the steel skeleton was introduced and allowed for the construction of tall building that could withstand high winds. We take for granted some of the other inventions that enabled people to live and work in skyscrapers. For example, few people realized that the telephone was necessary for vertical communication and that flush toilet and vacuum incinerator made waste disposal possible. Now as we entered the age of super skyscraper, some with more than 200 floors, we see the need for even more technological innovations. In the area of heating and cooling systems for example. For all their benefits, these super tall building do cause problems though. For one thing they place enormous train on parking and traffic row in urban areas. But let's leave behind these technical concerns and move on to consider some of the design elements that have come to characterize the age of the skyscraper.

40. What does the professor mainly discuss?
41. What does the professor say about the invention of the elevator?
42. What will the professor probably discuss next?

43 to 46 Talk in an introductory biology class.
Good morning. Let me welcome all the new members of the ski patrol. My name is Brenda Peters and I've been a ski-patroller for nearly three years. After working in offices and restaurants I find this job very satisfying. After all, if you love the outdoors, it's a great job. You won't get rich but you';; get in great shape and you will be able to help people everyday. And for me the best part of the job is giving ski lessons I've made a lot of friends that way. Of course your main responsibilities will involve ensuring the safety of everyone who skis here. All trails have to be checked daily to make sure they are groomed probably. You also will have to be watching out constantly for people who aren't skiing safely, or who may think they are better skiers than they really are. And to help those who are injured, you'll have to know basic first aid. Tomorrow you will begin training in a first aid system that skiers specifically to the outdoors. We pride ourselves in our ability to get people off the mountain quickly and safely. OK, so you are free to explore the slopes for the test of the day. All in all, I think that being a ski patroller is great job I hope you will all feel the same way.

43. What does the speaker mainly discuss?
44. According to the speaker, what is one advantage of working on a ski patrol?
45. What does the speaker say she likes most about giving ski lessons?
46. According to the speaker, what is one of the man responsibilities of ski patroller?

47-50 A lecture given in a geology class.
If you flew over certain parts of Nebraska and Texas by plane, you might notice some large areas appearing as bright green circles many hundreds of feet across. This green is unusual in the high plains area where the climate is very dry. These green patches are the result of a new technique for mining the underground water. In this technique, miners bore deep holes in the ground until they reach a special geological formation called ocheropher. The water which has collected in this ocherophers for hundreds of years in called fossil water or ground water. It pumped up through the bored hole and spray over the land to irrigate the crops. Raising crops such as cotton and wheat, water in this way creates the fertile green areas that contrasts vividly with the natural brown of the plains. Crop yields have increase dramatically. However they've created a serious environmental problem. The problem is that the water is being removed from many ocherophers faster than it can be replenished naturally. Ground water levels have dropped rapidly and it's becoming more difficult and expensive to get this water. In some parts of Texas, water levels in some of the ocherophers have declined cess of using water faster than it can be replaced is wide-spread and serious.

47. What does the speaker mainly discuss?
48. What benefits have resulted from the technique?
49. What is happening to ground water?
50. What does the speaker imply about ground water?


